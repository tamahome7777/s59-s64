/*import { Avatar, Rate, Space, Table, Typography, Button, Form, Input } from "antd";
import { useEffect, useState } from "react";
import { useParams } from 'react-router-dom';


export default function Inventory() {
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [isActive, setIsActive] = useState(true);
  const {id} = useParams();
  const [editingRow, setEditingRow] = useState(null);
  const [form] = Form.useForm();
  const [columns, setColumns]=useState('')

  useEffect(() => {
    setLoading(true);
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
    .then(response => response.json())
    .then(data => {
      console.log(data);

      setDataSource(data);
      setLoading(false);
    });
  }, []);

  return (
    <Space size={20} direction="vertical">
      <Typography.Title level={4}>Inventory</Typography.Title>
      <Table
        loading={loading}
        columns={[
          {
            title: "Thumbnail",
            dataIndex: "thumbnail",
            render: (link) => {
              return <Avatar src={link} />;
            },
          },
          {
            title: "Product Name",
            dataIndex: "productName",
            render: (text, data) => {
        if (editingRow === data._id) {
          return (
            <Form.Item
              name="productName"
            >
              <Input />
            </Form.Item>
          );
        } else {
          return <p>{text}</p>;
        }
      },
    
          },
          {
            title: "Product Description",
            dataIndex: "productDescription",
          render: (text, data) => {
                  if (editingRow === data._id) {
                    return (
                      <Form.Item
                        name="productDescription"
                      >
                        <Input />
                      </Form.Item>
                    );
                  } else {
                    return <p>{text}</p>;
                  }
                },
              
                    },
          {
            title: "Product Price",
            dataIndex: "productPrice",
            render: (text, data) => {
                  if (editingRow === data._id) {
                    return (
                      <Form.Item
                        name="productPrice"
                      >
                        <Input />
                      </Form.Item>
                    );
                  } else {
                    return <p>{text}</p>;
                  }
                },
              
                    },
          {
            title: "isActive",
            dataIndex: "isActive",
            render: (text, data) => {
                  if (editingRow === data._id) {
                    return (
                      <Form.Item
                        name="isActive"
                      >
                        <Input />
                      </Form.Item>
                    );
                  } else {
                    return <p>{text}</p>;
                  }
                },
              
                    },
          {
            title: "Rating",
            dataIndex: "rating",
            render: (rating) => {
              return <Rate value={rating}  />;
            },
          },
          {
            title: "Actions",
            render: (_, data) => {
              return (
                <>
                  <Button
                    type="link"
                    onClick={() => {
                      setEditingRow(data._id);
                      form.setFieldsValue({
                        productName:data.productName,
                        productDescription:data.productDescription,
                        productPrice:data.productPrice,
                      });
                    }}
                  >
                    Edit
                  </Button>
                  <Button type="link" htmlType="submit">
                    Save
                  </Button>
                </>
              );
            },
          },
          
        ]}
        dataSource={dataSource}
        pagination={{
          pageSize: 5,
        }}
      ></Table>
    </Space>
  );
  const onFinish = (data) => {
    const updatedDataSource = [dataSource];
    updatedDataSource.splice(editingRow, 1,  data._id );
    setDataSource(updatedDataSource);
    setEditingRow(null);
  };
  return (
    <div className='Inventory'>
      <header className='Inventory-header'>
         <Form form={form} onFinish={onFinish}>
        <Table columns={columns} dataSource={dataSource}></Table>
        </Form>
      </header>
    </div>
  );
}


*/

import { Button, Table, Form, Input } from "antd";
import { useParams } from 'react-router-dom';
import { useEffect,useState } from "react";


export default function Inventory() {
  const [dataSource, setDataSource] = useState([]);
  const [editingRow, setEditingRow] = useState(null);
  const [form] = Form.useForm();
  const [productName, setProductName] = useState('');
  const [productPrice, setProductPrice] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [product, setProduct] = useState('');
  const [loading, setLoading] = useState(false);
  const {id} = useParams();

/*  useEffect(() => {
    const data = [];
    for (let index = 0; index < 7; index++) {
      data.push({
        key: `${index}`,
        name: `Name ${index}`,
        address: `Address ${index}`,
      });
    }
    setDataSource(data);
  }, []);*/

useEffect(() => {
    setLoading(true);
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
    .then(response => response.json())
    .then(data => {
      console.log(data);
      data.push({

        productName: productName,
        productDescription: productDescription,
        productPrice: productPrice

      });
      console.log(data);
      setDataSource(data);
      setLoading(false);
    });

  }, []);

/*  
  //console.log(id);// product id

  useEffect(()=> {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
    .then(response => response.json())
    .then(data => {
       console.log(data);// product info
        setProductName(data.productName);
        setProductPrice(data.productPrice);
        setProductDescription(data.productDescription);
         setDataSource(data);
    })

  }, [])*/


  const columns = [
    {
      title: "Product Name",
      dataIndex: "productName",
      render: (text, record) => {
        if (editingRow === record._id) {
          return (
            <Form.Item
              name="productName"
              rules={[
                {
                  required: true,
                  message: "Please enter your name",
                },
              ]}
            >
              <Input />
            </Form.Item>
          );
        } else {
          return <p>{text}</p>;
        }
      },
    },
    {
      title: "Product Description",
      dataIndex: "productDescription",
      render: (text, record) => {
        if (editingRow === record._id) {
          return (
            <Form.Item name="productDescription">
              <Input />
            </Form.Item>
          );
        } else {
          return <p>{text}</p>;
        }
      },
    },
    {
      title: "Product Price",
      dataIndex: "productPrice",
      render: (text, record) => {
        if (editingRow === record._id) {
          return (
            <Form.Item name="productPrice">
              <Input />
            </Form.Item>
          );
        } else {
          return <p>{text}</p>;
        }
      },
    },
    {
      title: "Active",
      dataIndex: "isActive",
      render: (text, record) => {
        if (editingRow === record._id) {
          return (
            <Form.Item name="isActive">
              <Input />
            </Form.Item>
          );
        } else {
          return <p>{text}</p>;
        }
      },
    },
    {
      title: "Actions",
      render: (_, record) => {
        return (
          <>
            <Button
              type="link"
              onClick={() => {
                setEditingRow(record._id);
                form.setFieldsValue({
                  productName: record.productName,
                  productDescription: record.productDescription,
                  productPrice:record.productPrice,
                  isActive:record.isActive,
                });
              }}
            >
              Edit
            </Button>
            <Button type="link" htmlType="submit">
              Save
            </Button>
          </>
        );
      },
    },
  ];
  const onFinish = (record) => {
    console.log(record._id);
    const updatedDataSource = [dataSource];
    updatedDataSource.splice(editingRow, 1, record.id );
    setDataSource(updatedDataSource);
    //setEditingRow(null);
  };
  return (
    
    <div className="Inventory border">

      <header className="Inventory-header">
        <Form form={form} onFinish={onFinish}>
          <Table columns={columns} dataSource={dataSource} pagination={{
          pageSize: 5,
        }}></Table>
        </Form>
      </header>

    </div>
    

  );
}


