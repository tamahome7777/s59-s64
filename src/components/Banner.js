import { Button, Row, Col } from 'react-bootstrap';
import {Link } from 'react-router-dom';
export default function Banner(){
	return (
		<Row>
	    	<Col className="p-5 text-center">
	            <h1 >E-Commerce MERN App</h1>
	            <h4>3rd Capstone Project.</h4>
	            {/*<Button as = {Link} to = '/products' variant="primary">Buy now!</Button>*/}
	        </Col>
	    </Row>

	)
}